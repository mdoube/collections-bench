package de.heidelberg.pvs.container_bench.gscollections.maps;

import java.util.Map;

import org.eclipse.collections.impl.map.mutable.UnifiedMap;

import de.heidelberg.pvs.container_bench.abstracts.jdk.AbstractJDKMapTest;

public class GSCollections_LongInteger_UnifiedMaps_Test extends AbstractJDKMapTest<Long, Integer>{

	@Override
	protected Map<Long, Integer> getNewMap() {
		return new UnifiedMap<Long, Integer>();
	}

	@Override
	protected Map<Long, Integer> copyMap(Map<Long, Integer> fullMap2) {
		return new UnifiedMap<Long, Integer>(fullMap2);
	}

}
