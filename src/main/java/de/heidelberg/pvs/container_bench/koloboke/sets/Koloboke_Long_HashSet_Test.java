package de.heidelberg.pvs.container_bench.koloboke.sets;

import com.koloboke.collect.set.hash.HashObjSets;

import java.util.Set;

import de.heidelberg.pvs.container_bench.abstracts.jdk.AbstractJDKSetTest;

public class Koloboke_Long_HashSet_Test extends AbstractJDKSetTest<Long> {

	@Override
	protected Set<Long> getNewSet() {
		return HashObjSets.newMutableSet();
	}

	@Override
	protected Set<Long> copySet(Set<Long> fullSet2) {
		return HashObjSets.newMutableSet(fullSet2);
	}

}
