package de.heidelberg.pvs.container_bench.koloboke.sets;

import com.koloboke.collect.set.hash.HashObjSets;

import java.util.Set;

import de.heidelberg.pvs.container_bench.abstracts.jdk.AbstractJDKSetTest;

public class Koloboke_String_HashSet_Test extends AbstractJDKSetTest<String> {

	@Override
	protected Set<String> getNewSet() {
		return HashObjSets.newMutableSet();
	}

	@Override
	protected Set<String> copySet(Set<String> fullSet2) {
		return HashObjSets.newMutableSet(fullSet2);
	}
	

}
